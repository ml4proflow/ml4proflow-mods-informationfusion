.. ml4proflow Information Fusion Modules documentation master file, created by
   sphinx-quickstart on Thu Apr  7 11:27:35 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ml4proflow Information Fusion Modules' documentation.
=================================================================

.. image:: https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/tests-badge.svg?job=gen-cov
   :target: #CI
   :alt: Tests Status


.. image:: https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/coverage-badge.svg?job=gen-cov
   :target: #CI
   :alt: Coverage Status


.. image:: https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/flake8-badge.svg?job=gen-cov
   :target: #CI
   :alt: Flake8 Status


.. image:: https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/mypy.svg?job=gen-cov
   :target: #CI
   :alt: mypy errors



.. image:: https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/mypy_strict.svg?job=gen-cov
   :target: #CI
   :alt: mypy strict errors

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   informationfusion
   literature
