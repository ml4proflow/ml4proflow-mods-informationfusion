Package API
=============================
This package contains the following python modules:

- :ref:`modules_module`:

  Contains the ml4proflow modules and related functions.
- :ref:`widgets_module`:

  Contains the wrapper classes for GUI support.

.. _modules_module:

informationfusion.modules
--------------------------------

.. automodule:: informationfusion.modules
   :members:
   :undoc-members:
   :show-inheritance:

.. _widgets_module:

informationfusion.widgets
--------------------------------

.. automodule:: informationfusion.widgets
   :members:
   :undoc-members:
   :show-inheritance:
