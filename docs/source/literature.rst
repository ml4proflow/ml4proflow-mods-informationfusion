Literature
=============================
This section provides a non-exhaustive list of literature on fuzzy pattern
classification and related topics.

-----------------------------

.. [Boc87] Bocklisch, Steffen. *Prozeßanalyse mit unscharfen Verfahren*. Verlag Technik, 1987.

.. [Boc17] Bocklisch, Franziska, et al. "Adaptive fuzzy pattern classification for the online detection of driver lane change intention." *Neurocomputing 262* (2017): 148-158.

.. [BH18] Bocklisch, Franziska, and Daniel Hausmann. "Multidimensional fuzzy pattern classifier sequences for medical diagnostic reasoning." *Applied soft computing 66* (2018): 297-310.

.. [EML16] Ehlenbröker, Jan-Friedrich, Uwe Mönks, and Volker Lohweg. "Sensor defect detection in multisensor information fusion." *Journal of Sensors and Sensor Systems 5.2* (2016): 337-353.

.. [HB08] Hempel, Arne-Jens, and Steffen F. Bocklisch. "Hierarchical modelling of data inherent structures using networks of fuzzy classifiers." *Tenth International Conference on Computer Modeling and Simulation (uksim 2008)* (2008): 230-235.

.. [HB10] Hempel, Arne-Jens, and Steffen F. Bocklisch. *Fuzzy pattern modelling of data inherent structures based on aggregation of data with heterogeneous fuzziness*. INTECH Open Access Publisher, 2010.

.. [Hem11] Hempel, Arne-Jens. *Netzorientierte Fuzzy-Pattern-Klassifikation nichtkonvexer Objektmengenmorphologien.* Universitätsverlag Chemnitz, 2011.

.. [Hem12] Hempel, Arne-Jens, et al. "SVM-integrated Fuzzy Pattern Classification for Nonconvex Data-inherent Structures Applied to Banknote Authentication." *Bildverarbeitung in der Automation*. inIT, Lemgo (2012).

.. [HL18] Holst, Christoph-Alexander, and Volker Lohweg. "Supporting sensor orchestration in non-stationary environments." *Proceedings of the 15th ACM International Conference on Computing Frontiers* (2018): 363–370.

.. [HL19] Holst, Christoph-Alexander, and Volker Lohweg. "Feature fusion to increase the robustness of machine learners in industrial environments." *at-Automatisierungstechnik 67.10* (2019): 853-865.

.. [Lar99] Larsen, Henrik Legind. "Importance weighted OWA aggregation of multicriteria queries." *18th International Conference of the North American Fuzzy Information Processing Society-NAFIPS* (1999): 740-744.

.. [Lar02] Larsen, Henrik Legind. "Efficient importance weighted aggregation between min and max". *Ninth International Conference on Information Processing and Management of Uncertainty in Knowledge-Based Systems (IPMU 2002)*  (2002) 740-744.

.. _citeLDM04:

.. [LDM04] Lohweg, Volker, Carsten Diederichs, and Dietmar Müller. "Algorithms for hardware-based pattern recognition." *EURASIP Journal on Advances in Signal Processing 2004.12* (2004): 1-9.

.. [LM10a] Lohweg, Volker, and Uwe Mönks. "Fuzzy-pattern-classifier based sensor fusion for machine conditioning." *Sensor Fusion and its Applications* (2010): 319-346.

.. _citeLM10b:

.. [LM10b] Lohweg, Volker, and Uwe Mönks. "Sensor fusion by two-layer conflict solving." *2010 2nd International Workshop on Cognitive Information Processing* (2010): 370-375.

.. [Loh03] Lohweg, Volker. "Ein Beitrag zur effektiven Implementierung adaptiver Spektraltransformationen in applikationsspezifische integrierte Schaltkreise." *Dissertationsschrift an der Technischen Universität Chemnitz* (2003).

.. [MDL10] Mönks, Uwe, Denis Petker, and Volker Lohweg. "Fuzzy-pattern-classifier training with small data sets." *International Conference on Information Processing and Management of Uncertainty in Knowledge-Based Systems* (2010): 426–435.

.. [MLL09] Mönks, Uwe, Volker Lohweg, and Henrik Legind Larsen. "Aggregation Operator Based Fuzzy Pattern Classifier Design." *Lemgo Series on Industrial Information Technology 3* (2009).

.. _citeMön17:

.. [Mön17] Mönks, Uwe. *Information Fusion Under Consideration of Conflicting Input Signals*. Heidelberg: Springer Berlin Heidelberg, 2017.

.. [Sch93] Schlegel, P., et al. "Accelerated fuzzy pattern classification with ASICs." *Sixth Annual IEEE International ASIC Conference and Exhibit* (1993):  250-253.

.. [Vot11] Voth, Karl, et al. "Multi-Sensory Machine Diagnosis on Security Printing Machines with Two Layer Conflict Solving." *Proceedings of the SENSOR+TEST Conference 2011* (2011): 686–691.

.. [Yag88] Yager, Ronald R. "On ordered weighted averaging aggregation operators in multicriteria decisionmaking." *IEEE Transactions on systems, Man, and Cybernetics 18.1* (1988): 183-190.

.. [Yag99] Yager, Ronald R. "Nonmonotonic OWA operators." *Soft computing 3.3* (1999): 187-196.
