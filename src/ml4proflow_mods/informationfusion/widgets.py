from ml4proflow_jupyter.widgets import BasicWidget
from ml4proflow_mods.informationfusion.modules import (
    ModifiedFuzzyPatternTransformerModule,
    ModifiedFuzzyPatternClassifier,
    MACROAttribute,
    MultilayerAttributeBasedConflictReducingObservation
)
from ipywidgets import Output, DOMWidget, HBox

import numpy as np
import matplotlib.pyplot as plt


class MFPTransformerWidget(BasicWidget):
    def __init__(self, module: ModifiedFuzzyPatternTransformerModule):
        BasicWidget.__init__(self, module)
        self.plot_box = Output()
        self.train_box = Output()
        self.module = module
        self.module.config.setdefault('plotenabled', True)
        # Overwrite module on new data with widget on new data:
        self.module_on_new_data = self.module.on_new_data
        self.module.on_new_data = self.widget_on_new_data
        with self.plot_box:
            print('Module has not received any data yet.')
        with self.train_box:
            print('Module has not received any data yet.')

    def widget_on_new_data(self, name: str, sender, data) -> None:
        self.module_on_new_data(name, sender, data)

        if self.module.config['plotenabled']:
            X, Y = self.module.get_membership_functions_xy()
            nfeatures = X.shape[1]
            nrows = int(np.sqrt(nfeatures))
            ncols = nfeatures // nrows + 1
            fig, axs = plt.subplots(nrows, ncols, sharey='all',
                                    subplot_kw=dict(box_aspect=1))
            s = np.broadcast_to(self.module.get_as_farray('s'), nfeatures)
            cl = np.broadcast_to(self.module.get_as_farray('cl'), nfeatures)
            cr = np.broadcast_to(self.module.get_as_farray('cr'), nfeatures)
            for i in range(nfeatures):
                axs[i].plot(X[:, i], Y[:, i])
                axs[i].axvline(s[i] - cl[i], alpha=0.5, color='k')
                axs[i].axvline(s[i] + cr[i], alpha=0.5, color='k')
                axs[i].set_ylim([0, 1])
                axs[i].set_xlabel('Feature Value')
                axs[i].set_ylabel('Membership Value')
                axs[i].set_title(f'Feature {i}')

            for i in range(nfeatures, nrows*ncols):
                axs[i].set_visible(False)

            self.plot_box.clear_output()
            with self.plot_box:
                plt.tight_layout()
                plt.show(fig)

        self.train_box.clear_output()
        with self.train_box:
            if self.module.config['mode'] == 'fit':
                print(f"s: {self.module.config['s']}\n"
                      f"cl: {self.module.config['cl']}\n"
                      f"cr: {self.module.config['cr']}")
            else:
                print(f'Module is in {self.module.config["mode"]} mode.'
                      f'Configuration parameters are not updated.')

    def _get_plot_box(self):
        return HBox([self.plot_box])

    def _get_train_box(self):
        return HBox([self.train_box])

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [('Membership Functions', self._get_plot_box(),),
                ('Updated Configuration Parameters', self._get_train_box(),)]


class MFPClassifierWidget(BasicWidget):
    def __init__(self, module: ModifiedFuzzyPatternClassifier):
        BasicWidget.__init__(self, module)
        self.plot_box = Output()
        self.train_box = Output()
        self.module = module
        self.module.config.setdefault('plotenabled', True)
        # Overwrite module on new data with widget on new data:
        self.module_on_new_data = self.module.on_new_data
        self.module.on_new_data = self.widget_on_new_data
        with self.plot_box:
            print('Module has not received any data yet.')
        with self.train_box:
            print('Module has not received any data yet.')

    def widget_on_new_data(self, name: str, sender, data) -> None:
        self.module_on_new_data(name, sender, data)

        if self.module.config['plotenabled']:
            X, Y = self.module.mfpc_transformer.get_membership_functions_xy()
            nfeatures = X.shape[1]
            nrows = int(np.sqrt(nfeatures))
            ncols = nfeatures // nrows + 1
            fig, axs = plt.subplots(nrows, ncols, sharey='all',
                                    subplot_kw=dict(box_aspect=1))
            s = np.broadcast_to(
                self.module.mfpc_transformer.get_as_farray('s'), nfeatures)
            cl = np.broadcast_to(
                self.module.mfpc_transformer.get_as_farray('cl'), nfeatures)
            cr = np.broadcast_to(
                self.module.mfpc_transformer.get_as_farray('cr'), nfeatures)
            for i in range(nfeatures):
                axs[i].plot(X[:, i], Y[:, i])
                axs[i].axvspan(s[i] - cl[i], s[i] + cr[i],
                               alpha=0.5, color='g')
                axs[i].set_ylim([0, 1])
                axs[i].set_xlabel('Feature Value')
                axs[i].set_ylabel('Membership Value')
                axs[i].set_title(f'Feature {i}')

            for i in range(nfeatures, nrows*ncols):
                axs[i].set_visible(False)

            self.plot_box.clear_output()
            with self.plot_box:
                plt.tight_layout()
                plt.show(fig)

        self.train_box.clear_output()
        with self.train_box:
            if self.module.config['mode'] == 'fit':
                print(f"s: {self.module.config['s']}\n"
                      f"cl: {self.module.config['cl']}\n"
                      f"cr: {self.module.config['cr']}")
            else:
                print(f'Module is in {self.module.config["mode"]} mode.'
                      f'Configuration parameters are not updated.')

    def _get_plot_box(self):
        return HBox([self.plot_box])

    def _get_train_box(self):
        return HBox([self.train_box])

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [('Membership Functions', self._get_plot_box(),),
                ('Updated Configuration Parameters', self._get_train_box(),)]


class MACROAttributeWidget(BasicWidget):
    def __init__(self, module: MACROAttribute):
        BasicWidget.__init__(self, module)
        self.plot_box = Output()
        self.train_box = Output()
        self.module = module
        self.module.config.setdefault('plotenabled', True)
        # Overwrite module on new data with widget on new data:
        self.module_on_new_data = self.module.on_new_data
        self.module.on_new_data = self.widget_on_new_data
        with self.plot_box:
            print('Module has not received any data yet.')
        with self.train_box:
            print('Module has not received any data yet.')

    def widget_on_new_data(self, name: str, sender, data) -> None:
        self.module_on_new_data(name, sender, data)

        if self.module.config['plotenabled']:
            X, Y = self.module.mfpc_transformer.get_membership_functions_xy()
            nfeatures = X.shape[1]
            nrows = int(np.sqrt(nfeatures))
            ncols = nfeatures // nrows + 1
            fig, axs = plt.subplots(nrows, ncols, sharey='all',
                                    subplot_kw=dict(box_aspect=1))
            s = np.broadcast_to(
                self.module.mfpc_transformer.get_as_farray('s'), nfeatures)
            cl = np.broadcast_to(
                self.module.mfpc_transformer.get_as_farray('cl'), nfeatures)
            cr = np.broadcast_to(
                self.module.mfpc_transformer.get_as_farray('cr'), nfeatures)
            for i in range(nfeatures):
                axs[i].plot(X[:, i], Y[:, i])
                axs[i].axvline(s[i] - cl[i], alpha=0.5, color='k')
                axs[i].axvline(s[i] + cr[i], alpha=0.5, color='k')
                axs[i].set_ylim([0, 1])
                axs[i].set_xlabel('Feature Value')
                axs[i].set_ylabel('Membership Value')
                axs[i].set_title(f'Feature {i}')

            for i in range(nfeatures, nrows*ncols):
                axs[i].set_visible(False)

            self.plot_box.clear_output()
            with self.plot_box:
                plt.tight_layout()
                plt.show(fig)

        self.train_box.clear_output()
        with self.train_box:
            if self.module.config['mode'] == 'fit':
                print(f"s: {self.module.config['s']}\n"
                      f"cl: {self.module.config['cl']}\n"
                      f"cr: {self.module.config['cr']}")
            else:
                print(f'Module is in {self.module.config["mode"]} mode.'
                      f'Configuration parameters are not updated.')

    def _get_plot_box(self):
        return HBox([self.plot_box])

    def _get_train_box(self):
        return HBox([self.train_box])

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [('Membership Functions', self._get_plot_box(),),
                ('Updated Configuration Parameters', self._get_train_box(),)]


class MACROWidget(BasicWidget):
    def __init__(self,
                 module: MultilayerAttributeBasedConflictReducingObservation):
        BasicWidget.__init__(self, module)
        self.info_box = Output()
        self.module = module
        # Overwrite module on new data with widget on new data:
        self.module_on_new_data = self.module.on_new_data
        self.module.on_new_data = self.widget_on_new_data
        with self.info_box:
            print('Module has not received any data yet.')

    def widget_on_new_data(self, name: str, sender, data) -> None:
        self.module_on_new_data(name, sender, data)

        applied_andness = self.module.get_applied_andness(self.module.weights)
        self.info_box.clear_output()
        with self.info_box:
            print(f"The andness the module is working with is "
                  f"{applied_andness}.")

    def _get_info_box(self):
        return HBox([self.info_box])

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [('Parameter Information', self._get_info_box(),)]
