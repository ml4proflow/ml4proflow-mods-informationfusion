from __future__ import annotations  # For class references before definition

import warnings
from typing import Union, Any, Iterable, Callable, Tuple

import numpy
from pandas import DataFrame
import numpy as np
from numpy.typing import ArrayLike

from ml4proflow.modules import DataFlowManager, Module, SourceModule

_IFU_LITERATURE_HTML = """
For further information including the literature references, please refer to
the package documentation.
"""

# Conversion from docstring to HTML is not done automatically to reduce the
# package dependencies. After updating the docstring use a converter tool
# to update this string:
_MFPTRANSFORMER_PARAM_DESC_HTML = """
<li><code>'mode'</code>: Mode of operation. Either <code>'fit'</code>
(default) or <code>'predict'</code>.</li>
<li><code>'n_features'</code>: Number of features. If not set or a
negative value, the parameter is inferred during training. Default:
<code>-1</code>.</li>
<li><code>'parmethod'</code>: Method used to calculate the mode. The
options are:
<ul>
<li><code>'median'</code> The mode is determined using the median
(default).</li>
<li><code>'mean'</code> The mode is determined using the arithmetic
mean.</li>
<li><code>'classical'</code> Parameterization after [LDM04]. Should only
be used for symmetrical membership functions.</li>
</ul></li>
<li><code>'s'</code>: Mode position. Learnt during <code>'fit'</code>
mode. Default: <code>0</code>.</li>
<li><code>'cl'</code>: Left class border. Learnt during
<code>'fit'</code> mode. Default: <code>1</code>.</li>
<li><code>'cr'</code>: Right class border. Learnt during
<code>'fit'</code> mode. Default: <code>1</code>.</li>
<li><code>'bl'</code>: Left-hand side border membership. Note: For the
MFPC, bl = μ(s - cl) only holds if set to <code>0.5</code>
(default).</li>
<li><code>'br'</code>: Right-hand side border membership. Note: For the
MFPC, br = μ(s + cr) only holds if set to <code>0.5</code>
(default).</li>
<li><code>'dl'</code>, <code>'dr'</code>: Left-hand side and right-hand
side slope steepness. Default: <code>2</code>.</li>
<li><code>'pcel'</code>, <code>'pcer'</code>: Left-hand side and
right-hand side percental elementary fuzziness. Value between
<code>0</code> and <code>1</code>. Default: <code>0</code>.</li>
<li><code>'epsilon'</code>: Epsilon for clipping <code>cl</code> and
<code>cr</code>. If a parameter is below this threshold it is clipped to
the value of <code>epsilon</code>. Default: <code>10e-18</code>.</li>
"""


class MACROAttribute(Module):
    """
    This module implements the attribute layer fusion for the
    Multilayer Attribute-based Conflict-reducing Observation (MACRO)
    model published in [Mön17]_.

    :param dfm: The :py:class:`~ml4proflow.modules.DataFlowManager` this module
      is part of.
    :param config: The configuration containing the module parameters.

      For relevant configuration parameters, see
      :py:class:`ModifiedFuzzyPatternTransformer`.
    """

    def __init__(
        self,
        dfm: DataFlowManager,
        config: dict[str, Any]
    ):
        Module.__init__(self, dfm, config)

        # Parameters solely used by transformer:
        self.config = config
        self.config.setdefault('mode', 'fit')

        if self.config['mode'] not in ['fit', 'predict']:
            raise ValueError('Mode has to be "fit" or "predict".')

        self.mfpc_transformer = ModifiedFuzzyPatternTransformer(
            self.config, self.update_config)

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        """
        :return: The module description required by ml4proflow.
        """
        return {"name": "MACRO Attribute Module",
                "jupyter-gui-cls": "ml4proflow_mods.informationfusion.widgets."
                                   "MACROAttributeWidget",
                "html-description": """
    <h1 id="macro-attribute">Multilayer Attribute-based Conflict-reducing
    Observation (MACRO) Attribute</h1>
    <p>This module implements the attribute layer fusion for the
    Multilayer Attribute-based Conflict-reducing Observation (MACRO)
    model published in [Mön17].</p>

    <p>For further information on the module, refer to the documentation.</p>
    """ + _IFU_LITERATURE_HTML + """
    <h2 id="config-parameters">Configuration Parameters</h2>
    <ul>
    """ + _MFPTRANSFORMER_PARAM_DESC_HTML + """</ul>"""}

    def on_new_data(self, name: str, sender: SourceModule,
                    data: DataFrame) -> None:
        """Aggregate a new batch of data. If mode is ``'fit'`` the fuzzy
        pattern transformer is fitted with the data before prediction.

        The following :py:class:`pandas.DataFrame` is pushed to channel
        ``name``:

        +-------+-----------------+-----------------+
        |       | membership      | importance      |
        +=======+=================+=================+
        | **0** | ``float``-value | ``float``-value |
        +-------+-----------------+-----------------+
        | ...   | ``float``-value | ``float``-value |
        +-------+-----------------+-----------------+
        | **n** | ``float``-value | ``float``-value |
        +-------+-----------------+-----------------+

        :param name: Name of the channel.
        :param sender: Name of the module that sends the data.
        :param data: DataFrame to be processed. The underlying data is of shape
          (batch size, number of features).
        """
        X = data.values
        if self.config['mode'] == 'fit':
            self.fit(X)

        result = self.fuse_samples(X)
        result_df = DataFrame(data={
            'membership': result[:, 0],
            'importance': result[:, 1]
        })
        for c in self.config['channels_push']:
            self._push_data(c, result_df)

    def fit(self, X: np.ndarray) -> MACROAttribute:
        """
        This function fits the membership functions with ``X`` as training
        data. See :py:meth:`ModifiedFuzzyPatternTransformer.fit` for details.

        :return: ``self``
        """
        self.mfpc_transformer.fit(X)

        return self

    def fuse_samples(self, X: np.ndarray) -> np.ndarray:
        """
        For each feature, compute membership of the sample to the learnt
        fuzzy set. Afterwards, fuse the memberships of all features with
        fuzzified balanced tow-layer conflict solving (μBalTLCS) published in
        [:ref:`LM10b <citeLM10b>`, :ref:`Mön17 <citeMön17>`].

        :param X: Numpy array with shape
          (number of samples, number of features).
        :return: Numpy array with shape (number of samples, 2).

          - ``array[sample index, 0]``: Membership value of the sample.
          - ``array[sample index, 1]``: Importance value of the sample.
        """
        n_samples = X.shape[0]
        results = np.zeros((n_samples, 2))
        memberships = self.mfpc_transformer.transform(X)
        for i in range(n_samples):
            results[i, 0], results[i, 1] = fuzzified_bal_tlcs(memberships[i])
        return results


class MultilayerAttributeBasedConflictReducingObservation(Module):
    """
    This class implements the system layer fusion for the Multilayer
    Attribute-based Conflict-reducing Observation (MACRO) published in
    [Mön17]_.

    All inputs received by this module are expected to be send by
    :py:class:`MACROAttribute`\\ s. It is assumed that the number of input
    channels and the names do not change during operation.

    :param dfm: The :py:class:`~ml4proflow.modules.DataFlowManager` this module
      is part of.
    :param config: The configuration containing the module parameters.

      Configuration keys relevant for this module:

      - ``andness``: Given andness of the implicative importance weighted
        ordered weighted averaging (IIWOWA). This value is only an
        approximation of the applied andness. Value between ``0`` and ``1``.
        Default: ``0.5``.
      - ``batchsize``: The size of each batch of new data. Fixed sizes are
        required. For variable-sized batches, consider using a buffer in
        between the :py:class:`MACROAttribute`-Modules and this module to
        make the batch sizes fixed. Default: ``1``.
      - ``nattributes``: The number of :py:class:`MACROAttribute`\\ s sending
        to this module. Value greater or equal ``2``. Default: ``2``.
    """

    def __init__(
        self,
        dfm: DataFlowManager,
        config: dict[str, Any],
    ):
        Module.__init__(self, dfm, config)
        self.config = config
        self.config.setdefault('nattributes', 2)
        self.config.setdefault('batchsize', 1)
        self.config.setdefault('andness', 0.5)
        self.weights = self.get_weights(self.config['nattributes'],
                                        self.config['andness'])
        self.data_buffer: dict[Any, Any] = {}

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        """
        :return: The module description required by ml4proflow.
        """
        return {"name": "MACRO Module",
                "jupyter-gui-cls":
                    "ml4proflow_mods.informationfusion.widgets.MACROWidget",
                "html-description": """
    <h1 id="multilayer-attribute-based-conflict-reducing-observation-macro-">
    Multilayer Attribute-based Conflict-reducing Observation (MACRO)</h1>
    <p>This module implements the system layer fusion for the Multilayer
    Attribute-based Conflict-reducing Observation (MACRO) published in
    [Mön17].</p>
    <p>This module expects MACRO attributes as inputs (membership and
    importance values) and is the final layer of the information fusion.
    The MACRO attributes are aggregated with the implicative importance
    weighted ordered weighted average (IIWOWA). For details, please refer
    to the documentation.</p>
    """ + _IFU_LITERATURE_HTML + """
    <h2 id="config-parameters">Configuration Parameters</h2>
    <ul>
    <li><code>andness</code>: Given andness of the IIWOWA aggregation. This
    value is only an approximation of the applied andness. Value between
    <code>0</code> and <code>1</code>. Default: <code>0.5</code>.</li>
    <li><code>batchsize</code>: The size of each batch of new data. Fixed
    sizes re required. For variable-sized batches, consider using a buffer in
    between the <code class="interpreted-text"
    role="class">MACROAttribute</code><span class="title-ref">-Modules and
    this module to make the batch sizes fixed. Default: </span><span
    class="title-ref">1</span>.</li>
    <li><code>nattributes</code>: The number of <code
    class="interpreted-text" role="class">MACROAttribute</code><span
    class="title-ref">s sending to this module. Value greater or equal
    </span><span class="title-ref">2</span><span class="title-ref">.
    Default: </span><span class="title-ref">2</span>.</li>
    </ul>"""}

    @staticmethod
    def get_weights(n: int, andness: float) -> np.ndarray:
        """
        Get the weights of the implicative importance weighted ordered weighted
        averaging (IIWOWA).

        :param n: The number of inputs to the IIWOWA.
        :param andness: The given andness is an approximation of the
          applied andness. The applied andness can be calculated with
          :py:meth:`get_applied_andness`. Value between ``0`` and ``1``.

        :return: The weights for IIWOWA approximating the given
          andness.
        """
        return regular_monotonic_owa_quantifier_function(andness, n)

    @staticmethod
    def get_applied_andness(weights: ArrayLike) -> float:
        """
        Get the andness applied during implicative importance weighted ordered
        weighted averaging (IIWOWA).

        :param weights: The weights of the IIWOWA.

        :return: The andness applied during IIWOWA.
        """
        return compute_andness(weights)

    def on_new_data(self, name: str, sender: SourceModule,
                    data: DataFrame) -> None:
        """A :py:class:`MACROAttribute` sends a new batch of data. The system
        health is recalculated.

        The following :py:class:`pandas.DataFrame` is pushed to channel
        ``name``:

        +-------+-----------------+
        |       | system_health   |
        +=======+=================+
        | **0** | ``float``-value |
        +-------+-----------------+
        | ...   | ``float``-value |
        +-------+-----------------+
        | **n** | ``float``-value |
        +-------+-----------------+

        :param name: Name of the channel.
        :param sender: Name of the module that sends the data.
        :param data: DataFrame to be processed. The underlying data is a numpy
          array with shape (batch size, 2).

          - ``array[sample index, 0]``: Membership value of the sample.
          - ``array[sample index, 1]``: Importance value of the sample.
        """
        X = data.values

        # Check correct batch size
        if X.shape[0] != self.config['batchsize']:
            raise ValueError(f'New data on channel {name} has a batch size of '
                             f'{X.shape[0]} instead of '
                             f'{self.config["batchsize"]}.')

        self.data_buffer[name] = X

        # Check correct number of attributes sending
        if len(self.data_buffer.keys()) > self.config['nattributes']:
            raise ValueError(f'More than {self.config["nattributes"]} '
                             f'channels sending data to the module.')
        elif len(self.data_buffer.keys()) == self.config['nattributes']:
            system_health = self.get_system_health(self.data_buffer.values())
            result = DataFrame(data={'system_health': system_health})
            for c in self.config['channels_push']:
                self._push_data(c, result)

    def get_system_health(
            self, X_per_attribute: Iterable[ArrayLike]) -> np.ndarray:
        """
        Perform system layer fusion.

        :param X_per_attribute: Iterable of
          (number of samples, 2) array-likes. The following
          structure is expected for each array-like:

          - ``array[sample idx, 0]``: Membership value of the sample.
          - ``array[sample idx, 1]``: Importance value of the sample.
        :return: The system health for each sample. Numpy array with shape
          (n_samples,).
        """
        n_samples = self.config['batchsize']
        attribute_results = np.stack(list(X_per_attribute), axis=0)
        system_health = np.zeros(n_samples)
        # System layer fusion:
        for i in range(n_samples):
            # Memberships and importance of each attribute:
            memberships = attribute_results[:, i, 0]
            importance = attribute_results[:, i, 1]
            system_health[i] = implicative_importance_weighted_owa(
                importance, self.weights, memberships
            )
        return system_health


def fuzzified_bal_tlcs(memberships: ArrayLike) -> tuple[float, float]:
    """
    This function implements the fuzzified balanced two-layer conflict solving
    (μBalTLCS) published in
    [:ref:`LM10b <citeLM10b>`, :ref:`Mön17 <citeMön17>`]_.

    :param memberships: The fuzzy memberships to be fused. Fuzzy
      memberships must be in the interval [``0``, ``1``].
    :return: (membership, importance). A tuple of the resulting fuzzy
      membership value and its importance value.
    """
    # Check inputs:
    memberships = np.array(memberships)
    if memberships.ndim != 1:
        raise ValueError(
            "Membership array-like is expected to be one-dimensional.")
    if np.any(np.logical_or(memberships < 0, memberships > 1)):
        raise ValueError("Membership values must be in range [0, 1].")

    # Equation references can be found in [Mön17].
    # Fuzzy Membership - non-conflicting part (Eq. 4.31):
    n = len(memberships)
    mu_nc: float = 0
    for s in range(n - 1):
        for t in range(s + 1, n):
            mu_nc += memberships[s] * memberships[t]
    mu_nc *= 2.0 / (n * (n - 1))
    # Conflicting coefficient (Eq. 4.37):
    c = 2 * (np.mean(memberships) - mu_nc)
    # Fuzzy Membership - conflicting part (Eq. 4.32):
    mu_c = c * np.mean(memberships)
    # Combined Fuzzy Membership (Eq. 4.30):
    mu = mu_nc + mu_c
    # Importance (Eq. 4.42):
    importance = 1 - c
    return mu, importance


def implicative_importance_weighted_owa(
    importance: ArrayLike, w: ArrayLike, mu: ArrayLike
) -> float:
    """
    This function implements the implicative importance weighted ordered
    weighted averaging published in [Lar02]_.

    :param importance: The importance for each fuzzy memberships.
      Importance must be in the interval [``0``, ``1``].
    :param w: Weights for the weighted averaging. Weights must be in the
      interval [``0``, ``1``] and their sum must be equal to ``1``.
    :param mu: The fuzzy memberships to be aggregated. Fuzzy memberships must
      be in the interval [``0``, ``1``].
    :return: The aggregated fuzzy membership.
    """
    # Check inputs:
    importance = np.array(importance)
    w = np.array(w)
    mu = np.array(mu)
    mu[np.isclose(mu, 0)] = 0
    mu[np.isclose(mu, 1)] = 1
    if importance.ndim != 1:
        raise ValueError("Importance weighting array-like is expected to be "
                         "one-dimensional.")
    if w.ndim != 1:
        raise ValueError("Weight array-like is expected to be "
                         "one-dimensional.")
    if np.any(np.logical_or(w < 0, w > 1)):
        raise ValueError("Weights must be in range [0, 1].")
    if not np.allclose(np.sum(w), 1.0):
        raise ValueError("Sum of weights must be 1.")
    if mu.ndim != 1:
        raise ValueError(
            "Membership array-like is expected to be one-dimensional.")
    if np.any(np.logical_or(mu < 0, mu > 1)):
        raise ValueError("Membership values must be in range [0, 1].")
    if len(importance) != len(w) or len(w) != len(mu):
        raise ValueError(
            "Number of importance weightings, weights, and membership values "
            "must be equal.")

    # Equation references can be found in [Mön17].
    n = len(mu)
    iwowa_mu = importance_weighted_owa(importance, w, mu)
    iwowa_zeros = importance_weighted_owa(importance, w, np.zeros(n))
    iwowa_ones = importance_weighted_owa(importance, w, np.ones(n))
    # Calculate implicative importance weighted input fuzzy memberships
    # (Eq. 3.38):
    return (iwowa_mu - iwowa_zeros) / (iwowa_ones - iwowa_zeros)


def importance_weighted_owa(
    importance: ArrayLike, w: ArrayLike, mu: ArrayLike
) -> float:
    """
    This function implements the importance weighted ordered weighted
    averaging published in [Lar99]_.

    :param importance: The importance weighting of the fuzzy memberships.
      Importance must be in the interval [``0``, ``1``].
    :param w: Weights for the weighted averaging. Weights must be in the
      interval [``0``, ``1``] and their sum must be equal to ``1``.
    :param mu: The fuzzy memberships to be aggregated. Fuzzy memberships must
      be in the interval [``0``, ``1``].
    :return: The aggregated fuzzy membership.
    """
    # Check inputs:
    importance = np.array(importance)
    w = np.array(w)
    mu = np.array(mu)
    mu[np.isclose(mu, 0)] = 0
    mu[np.isclose(mu, 1)] = 1
    if importance.ndim != 1:
        raise ValueError("Importance weighting array-like is expected to be "
                         "one-dimensional.")
    if w.ndim != 1:
        raise ValueError(
            "Weight array-like is expected to be one-dimensional.")
    if np.any(np.logical_or(w < 0, w > 1)):
        raise ValueError("Weights must be in range [0, 1].")
    if not np.allclose(np.sum(w), 1.0):
        raise ValueError("Sum of weights must be 1.")
    if mu.ndim != 1:
        raise ValueError(
            "Membership array-like is expected to be one-dimensional.")
    if np.any(np.logical_or(mu < 0, mu > 1)):
        raise ValueError("Membership values must be in range [0, 1].")
    if len(importance) != len(w) or len(w) != len(mu):
        raise ValueError(
            "Number of importance weightings, weights, and membership values "
            "must be equal."
        )

    # Equation references can be found in [Mön17].
    andness = compute_andness(w)
    # Calculate importance weighted input fuzzy memberships (Eq. 3.36):
    b = andness + importance * (mu - andness)
    return ordered_weighted_averaging(w, b)


def ordered_weighted_averaging(w: ArrayLike, mu: ArrayLike) -> float:
    """
    This function implements the ordered weighted averaging published in
    [Yag88]_.

    :param w: Weights for the ordered weighted averaging. Weights must be in
      the interval [``0``, ``1``] and their sum must be equal to ``1``.
    :param mu: The fuzzy memberships to be aggregated. Fuzzy
      memberships must be in the interval [``0``, ``1``].
    :return: The aggregated fuzzy membership.
    """
    # Check inputs:
    w = np.array(w)
    mu = np.array(mu)
    mu[np.isclose(mu, 0)] = 0
    mu[np.isclose(mu, 1)] = 1
    if w.ndim != 1:
        raise ValueError(
            "Weight array-like is expected to be one-dimensional.")
    if np.any(np.logical_or(w < 0, w > 1)):
        raise ValueError("Weights must be in range [0, 1].")
    if not np.allclose(np.sum(w), 1.0):
        raise ValueError("Sum of weights must be 1.")
    if mu.ndim != 1:
        raise ValueError(
            "Membership array-like is expected to be one-dimensional.")
    if np.any(np.logical_or(mu < 0, mu > 1)):
        raise ValueError("Membership values must be in range [0, 1].")
    if len(w) != len(mu):
        raise ValueError(
            "Number of weights and membership values must be equal.")

    # See [Mön17] Eq. 3.30:
    # Sort memberships in descending order:
    mu = np.flip(np.sort(mu))
    # Aggregate:
    return float(np.dot(w, mu))


def regular_monotonic_owa_quantifier_function(
    quantifier_andness: float, n: int
) -> np.ndarray:
    """
    This function implements the regular monotonic ordered weighted averaging
    quantifier function published in [Yag99]_. This function computes a weight
    vector which approximates the given andness if used with the ordered
    weighted averaging.

    :param quantifier_andness: The quantifier andness is used by the
         quantifier function to calculate weights which approximate the ordered
         weighted averaging operator's andness. The andness is a value in the
         range [``0``, ``1``].
    :param n: The length of the resulting weight vector. Must be at least
      ``2``.
    :return: The weight vector of length n to be used with the ordered weighted
      averaging.
    """
    # Check inputs:
    if quantifier_andness < 0 or quantifier_andness > 1:
        raise ValueError("Quantifier andness must be in range [0, 1].")
    if n < 2:
        raise ValueError("Length n must be greater or equal 2.")

    # Check for min/max operators:
    weights = np.zeros(n)
    if np.allclose(quantifier_andness, 0):
        # Max operator, full orness:
        weights[0] = 1
    elif np.allclose(quantifier_andness, 1):
        # Min operator, full andness:
        weights[-1] = 1
    else:
        # Something in between. Use quantifier function.
        # Equation references can be found in [Mön17].
        # Calculate beta (Eq. 3.34):
        beta = quantifier_andness / (1 - quantifier_andness)
        # Calculate weights (Eq. 3.35):
        for i in range(n):
            # Caution:! In [Mön17] i = 1...n, here i = 1...n-1
            weights[i] = ((i + 1) / n) ** beta - (i / n) ** beta
    return weights


def compute_andness(weights: ArrayLike) -> float:
    """
    Calculate the andness of a vector of ordered weighted averaging weights
    as published in [Yag88]_.

    :param weights: The weights for which to calculate the andness.
    :return: The andness of the given weights.
    """
    # Check inputs:
    weights = np.array(weights)
    if weights.ndim != 1:
        raise ValueError("Weight array-like is expected to be "
                         "one-dimensional.")
    if np.any(np.logical_or(weights < 0, weights > 1)):
        raise ValueError("Weights must be in range [0, 1].")
    if not np.allclose(np.sum(weights), 1.0):
        raise ValueError("Sum of weights must be 1.")

    # Equation references can be found in [Mön17].
    n = len(weights)
    reversed_idx = np.flip(np.arange(n))
    # Compute orness (Eq. 3.32):
    orness = 1.0 / (n - 1) * np.dot(reversed_idx, weights)

    return 1 - orness


class ModifiedFuzzyPatternClassifier(Module):
    """
    This class implements the modified fuzzy pattern classifier (MFPC)
    published in [LDM04]_.

    :param dfm: The :py:class:`~ml4proflow.modules.DataFlowManager` this module
      is part of.
    :param config: The configuration containing the module parameters.

      Configuration keys relevant for this module:

      - ``'threshold'``: Threshold for class membership decision. Value
        between ``0`` and ``1``. Default: ``0.5``.
      - For all other parameters, see
        :py:class:`ModifiedFuzzyPatternTransformer`.
    """

    def __init__(
        self,
        dfm: DataFlowManager,
        config: dict[str, Any]
    ):
        Module.__init__(self, dfm, config)
        # The following numpy arrays are one dimensional and of the same length
        # (number of features):
        # Attributes added during training:
        # self.s_ : Center, e.g. mean, median or similar
        # self.cl_ : Left class border
        # self.cr_ : Right class border
        self.config = config
        self.config.setdefault('threshold', 0.5)
        self.config.setdefault('mode', 'fit')

        if self.config['mode'] not in ['fit', 'predict']:
            raise ValueError('Mode has to be "fit" or "predict".')

        self.mfpc_transformer = ModifiedFuzzyPatternTransformer(
            self.config, self.update_config)

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        """
        :return: The module description required by ml4proflow.
        """
        return {"name": "Modified Fuzzy Pattern Classifier",
                "jupyter-gui-cls": "ml4proflow_mods.informationfusion.widgets."
                                   "MFPClassifierWidget",
                "html-description": """
    <h1 id="modified-fuzzy-pattern-classifier">
    Modified Fuzzy Pattern Classifier</h1>
    <p>This module implements the modified fuzzy pattern classifier published
    in [LDM04].</p>
    """ + _IFU_LITERATURE_HTML + """
    <h2 id="config-parameters">Configuration Parameters</h2>
    <ul>
    <li><span class="title-ref">'threshold'</span><span class="title-ref">:
    Threshold for class membership decision. Value between </span><span
    class="title-ref">0</span><span class="title-ref"> and </span><span
    class="title-ref">1</span><span class="title-ref">. Default:
    </span><span class="title-ref">0.5</span>`.</li>
    """ + _MFPTRANSFORMER_PARAM_DESC_HTML + """</ul>"""}

    def on_new_data(self, name: str, sender: SourceModule,
                    data: DataFrame) -> None:
        """Classify a new batch of data. If mode is ``'fit'`` the fuzzy pattern
        transformer is fitted with the data before prediction.

        The following :py:class:`pandas.DataFrame` is pushed to channel
        ``name``:

        +-------+-----------------+
        |       | predicted_class |
        +=======+=================+
        | **0** | ``-1`` or ``1`` |
        +-------+-----------------+
        | ...   | ``-1`` or ``1`` |
        +-------+-----------------+
        | **n** | ``-1`` or ``1`` |
        +-------+-----------------+

        :param name: Name of the channel.
        :param sender: Name of the module that sends the data.
        :param data: DataFrame to be processed. The underlying data has the
          shape (batch size, number of features).
        """
        X = data.values
        if self.config['mode'] == 'fit':
            self.fit(X)

        result = DataFrame(data={'predicted_class': self.predict(X)})
        for c in self.config['channels_push']:
            self._push_data(c, result)

    def fit(self, X: np.ndarray) -> ModifiedFuzzyPatternClassifier:
        """
        This function fits the membership functions with ``X`` as training
        data. See :py:meth:`ModifiedFuzzyPatternTransformer.fit` for details.

        :raise ValueError: If the threshold is not in the interval
          [``0``, ``1``].
        :return: ``self``
        """
        # Check for valid threshold.
        # Other input checks are done by transformer.
        if self.config['threshold'] < 0 or self.config['threshold'] > 1:
            raise ValueError(
                "Threshold for decision should be between 0 and 1.")
        self.mfpc_transformer.fit(X)

        return self

    def predict(self, X: np.ndarray) -> np.ndarray:
        """
        Predict class membership for each sample.

        :param X: Numpy array with shape
          (number of samples, number of features).
        :return: Numpy array with shape (number of samples,). An entry is
          either ``1`` (inliers) or ``-1`` (outliers).
        """
        scores = self.score_samples(X)
        predictions = ((scores >= self.config['threshold']) - 0.5) * 2
        return np.array(predictions).astype(int)

    def decision_function(self, X: np.ndarray) -> np.ndarray:
        """
        Normalized inlier score. The values of the decision function are the
        shifted membership values of ``X`` so that a zero threshold can be used
        to detect an outlier.

        :param X: Numpy array with shape
          (number of samples, number of features). Samples for which to
          calculate the score.
        :return: Numpy array with shape (number of samples,). Normalized inlier
          score per sample.
        """
        return self.score_samples(X) - self.config['threshold']

    def score_samples(self, X: np.ndarray) -> np.ndarray:
        """
        Compute fuzzy membership for each sample. First, the membership is
        calculated per feature (see
        :py:meth:`ModifiedFuzzyPatternTransformer.transform` for details).
        Next, the memberships of all features are aggregated to a class
        membership according to [LDM04]_.

        :param X: Numpy array with shape
          (number of samples, number of features). Samples for which to
          calculate the score.
        :return: Numpy array with shape (number of samples,). Computed class
          memberships.
        """
        # Calculate membership exponent per sample and feature:
        memberships_per_feature = self.mfpc_transformer.transform(X)
        # Return result per sample (sum of exponents => product of elements):
        return np.prod(memberships_per_feature, axis=1)


class ModifiedFuzzyPatternTransformer:
    """
    This class implements the one-dimensional modified
    fuzzy pattern classifier (MFPC <mfpc>) membership function training.
    The learnt membership functions can also be used 'standalone' as a
    transformation of features to fuzzy membership values. The modified fuzzy
    pattern classifier is published in [LDM04]_.

    Parts of this class are loosely based on the MATLAB implementation of the
    MFPC by Uwe Mönks and Christoph-Alexander Holst.

    :param config: The configuration containing the module parameters.
      If a parameter is a float or a list with length one and there is more
      than one feature to be transformed, the same parameter value is used for
      every feature. Otherwise, a list of parameter values, one element for
      each feature, has to be given.

      Configuration keys relevant for this module:

      - ``'mode'``: Mode of operation. Either ``'fit'`` (default) or
        ``'predict'``.
      - ``'n_features'``: Number of features. If not set or a negative value,
        the parameter is inferred during training. Default: ``-1``.
      - ``'parmethod'``: Method used to calculate the mode. The options are:

        - ``'median'``: The mode is determined using the median (default).
        - ``'mean'``: The mode is determined using the arithmetic mean.
        - ``'classical'``: Parameterization after [LDM04]_. Should only be used
          for symmetrical membership functions.
      - ``'s'``: Mode position. Learnt during ``'fit'`` mode. Default: ``0``.
      - ``'cl'``: Left class border. Learnt during ``'fit'`` mode.
        Default: ``1``.
      - ``'cr'``: Right class border. Learnt during ``'fit'`` mode.
        Default: ``1``.
      - ``'bl'``: Left-hand side border membership. Note: For the MFPC,
        bl = μ(s - cl) only holds if set to ``0.5`` (default).
      - ``'br'``: Right-hand side border membership. Note: For the MFPC,
        br = μ(s + cr) only holds if set to ``0.5`` (default).
      - ``'dl'``, ``'dr'``: Left-hand side and right-hand side slope steepness.
        Default: ``2``.
      - ``'pcel'``, ``'pcer'``: Left-hand side and right-hand side percental
        elementary fuzziness. Value between ``0`` and ``1``. Default: ``0``.
      - ``'epsilon'``: Epsilon for clipping ``cl`` and ``cr``. If a parameter
        is below this threshold it is clipped to the value of ``epsilon``.
        Default: ``10e-18``.
    :param update_config: Function which is called to updated key-value
      pairs in the configuration dict. The trainable parameters
      are updated in the config after each training by calling this
      function. The function is called with the parameters (key, value).
    """
    def __init__(self, config: dict[str, Any],
                 update_config: Callable = None):
        # The following numpy arrays are one dimensional and of the same length
        # (number of features):
        # Config parameters/attributes updated during training:
        # s : Center, e.g. mean, median or similar
        # cl : Left class border
        # cr : Right class border
        self.config = config
        self.config.setdefault('mode', 'fit')
        self.config.setdefault('parmethod', 'median')
        self.config.setdefault('epsilon', 10e-18)
        shape = self.config.setdefault('nfeatures', -1)  # -1 = uninitialized
        if shape == 0:
            raise ValueError('At least one feature is required as input.')
        elif shape < 0:
            shape = 1
        self.config.setdefault('bl', [0.5] * shape)
        self.config.setdefault('br', [0.5] * shape)
        self.config.setdefault('dl', [2.0] * shape)
        self.config.setdefault('dr', [2.0] * shape)
        self.config.setdefault('pcel', [0] * shape)
        self.config.setdefault('pcer', [0] * shape)
        self.config.setdefault('s', [0.0] * shape)
        self.config.setdefault('cl', [1.0] * shape)
        self.config.setdefault('cr', [1.0] * shape)

        if update_config is None:
            update_config = self.update_config_default
        self.update_config = update_config

    def get_as_farray(self, key: str) -> np.ndarray:
        """
        Get the config parameters as numpy float array. This allows config
        values to be represented as strings.

        :param key: The key of the parameter in the configuration.
        :return: The value converted to a numpy float array.
        """
        return np.asfarray(self.config[key])

    def update_config_default(self, key: str, value: Any):
        """
        Default function called to update parameters in the config. This is
        done at the end of the training.

        :param key: The key of the configuration to be updated.
        :param value: The new value.
        """
        self.config[key] = value

    def _parameters_symmetrical(self) -> bool:
        """
        Checks if all parameters for the left-hand and right-hand sides are the
        same. This means that the membership functions are symmetrical. This
        method uses ``numpy.allclose()`` for the parameter comparisons.

        :return: True if parameters are symmetrical.
        """
        is_symmetrical = True
        for lpar, rpar in zip(
            [
                self.get_as_farray('bl'),
                self.get_as_farray('dl'),
                self.get_as_farray('pcel')
            ],
            [
                self.get_as_farray('br'),
                self.get_as_farray('dr'),
                self.get_as_farray('pcer')
            ]
        ):
            if not np.allclose(lpar, rpar):
                is_symmetrical = False

        return is_symmetrical

    def fit(self, X: np.ndarray) -> ModifiedFuzzyPatternTransformer:
        """
        Train the MFPC membership functions, i.e. calculate mode (``s``) and
        left-hand and right-hand side class borders (``cl``, ``cr``) based on
        the given training data ``X``.

        :param X: Array with shape (number of samples, number of features).
          The samples (objects) with which the MFPC membership functions are
          trained. NaNs will be ignored. A single column must contain at least
          one value which is not NaN.
        :return: ``self``
        """
        X = check_array(X)
        # Infer n_features:
        self.update_config('nfeatures',  X.shape[1])
        # Check for valid inputs:
        self._check_inputs()

        # Fit estimator: ------------------------------------------------------
        # Calculate values for parameters s_, cl_, cr_:
        featmin = np.nanmin(X, axis=0)
        featmax = np.nanmax(X, axis=0)
        pcel = np.broadcast_to(self.get_as_farray('pcel'),
                               (self.config['nfeatures'],))
        pcer = np.broadcast_to(self.get_as_farray('pcer'),
                               (self.config['nfeatures'],))

        if self.config['parmethod'] == "classical":  # Symmetric classifier
            if not self._parameters_symmetrical():
                warnings.warn(
                    "Classifier trained with classical approach but membership"
                    " functions are not symmetrical."
                )
            delta_c = (featmax - featmin) / 2.0
            s = delta_c + featmin
            cl = delta_c * (1 + 2 * pcel)
            cr = delta_c * (1 + 2 * pcer)
        else:  # Asymmetric membership functions
            if self.config['parmethod'] == "mean":
                s = np.nanmean(X, axis=0)
            elif self.config['parmethod'] == "median":
                s = np.nanmedian(X, axis=0)
            else:  # Double check for readability. Should never end up here.
                raise NotImplementedError(
                    "Parameterization not implemented yet.")
            delta_cc = featmax - featmin
            cl = s - featmin + pcel * delta_cc
            cr = featmax - s + pcer * delta_cc

        # Prevent infinitesimal small C parameters
        cl[cl < self.config['epsilon']] = self.config['epsilon']
        cr[cr < self.config['epsilon']] = self.config['epsilon']

        # Training done. Apply pce and store parameters in instance variables:
        self.update_config('s', s.tolist())
        self.update_config('cl', cl.tolist())
        self.update_config('cr', cr.tolist())

        return self

    def _check_inputs(self) -> None:
        """
        Input validation. The following checks are performed:

        - ``'parmethod'`` is  ``'median'``, ``'mean'`` or ``'classical'``.
        - The parameters ``'bl'``, ``'br'``, ``'dl'``, ``'dr'``, ``'pcel'``,
          and``'pcer'`` are scalars, arrays of length 1 or with the same length
          as the number of features. This means they can be broadcast to the
          number of features.

        :raise ValueError: If any of the checks failed.
        """
        # Check for valid hyperparameters:
        if self.config['parmethod'] not in ["median", "mean", "classical"]:
            raise ValueError(
                "Please specify a valid option for the parameterization."
                " Valid options are: 'median', 'mean', 'classical'."
            )
        for par in [self.get_as_farray('bl'), self.get_as_farray('br'),
                    self.get_as_farray('dl'), self.get_as_farray('dr'),
                    self.get_as_farray('pcel'), self.get_as_farray('pcer')]:
            if par.ndim > 1:  # Unexpected ndims
                raise ValueError(
                    "The hyperparameters must be a float or an array-like"
                    " with a dimension of one."
                )
            elif (par.ndim == 1 and
                  self.config['nfeatures'] != len(par) and
                  len(par) != 1):
                # Conflicting number of features
                raise ValueError(
                    "Hyperparameters must be either a float or specified"
                    " for each feature. Inconsistency of lengths (number"
                    " of features) between hyperparameters detected."
                )

    def _check_for_transformation(self, X: np.ndarray) -> np.ndarray:
        """
        Check if the number of features in ``X`` is the same
        as the number of features this transformer is expecting. If the number
        of expected features is not known yet it is set to the number of
        features in ``X``.

        :param X: Array-like of shape (number of samples, number of features).
        :raise ValueError: If the check fails.
        :return: ``X`` as :py:class:``numpy.ndarray``.
        """
        X = check_array(X)
        if self.config['nfeatures'] == -1:
            self.update_config('nfeatures', X.shape[1])
        if X.shape[1] != self.config['nfeatures']:
            raise ValueError(
                "Inconsistency of length (number of features) between "
                "provided samples and training samples."
            )

        return X

    def transform(self, X: np.ndarray) -> np.ndarray:
        """
        For each feature and sample in ``X``, compute the membership using the
        learnt membership functions.

        :param X: Array with shape (number of samples, number of features).
          The samples (objects) which will be transformed.
        :return: Array with the same shape as ``X``. The memberships.
        """
        X = self._check_for_transformation(X)

        # Broadcast arrays to support multiple objects:
        new_shape = X.shape
        cl = np.broadcast_to(self.get_as_farray('cl'), new_shape)
        cr = np.broadcast_to(self.get_as_farray('cr'), new_shape)
        s = np.broadcast_to(self.get_as_farray('s'), new_shape)
        bl = np.broadcast_to(self.get_as_farray('bl'), new_shape)
        br = np.broadcast_to(self.get_as_farray('br'), new_shape)
        dl = np.broadcast_to(self.get_as_farray('dl'), new_shape)
        dr = np.broadcast_to(self.get_as_farray('dr'), new_shape)

        # Compute difference measures feature-wise:
        distances = np.zeros(X.shape)
        # Elements right to s_:
        l_idx = X < s
        distances[l_idx] = self._compute_distance(
            X[l_idx], s[l_idx], cl[l_idx], bl[l_idx], dl[l_idx]
        )
        # Elements right to s_:
        r_idx = X >= s
        distances[r_idx] = self._compute_distance(
            X[r_idx], s[r_idx], cr[r_idx], br[r_idx], dr[r_idx]
        )
        # Return result per sample and per feature:
        return 2 ** (-distances)

    @staticmethod
    def _compute_distance(
        features: np.ndarray,
        s: np.ndarray,
        c: np.ndarray,
        b: np.ndarray,
        d: np.ndarray,
    ) -> np.ndarray:
        """
        Compute the fuzzy pattern classifier difference measure d(m;p). See
        [LDM04]_.
        - Symmetrical membership function: Provide all features.
        - Asymmetrical membership function: Provide features for the
          left-hand and right-hand side in different calls with the appropriate
          function parameters.

        :param features: One-dimensional :py:class:``numpy.ndarray`` array.
          Feature values of an object for which the difference measures are
          computed.
        :param s: One dimensional array with the same length as ``features``.
          Mode centers, feature-wise.
        :param c: One dimensional array with the same length as ``features``.
          Class borders, feature-wise.
        :param b: One dimensional array with the same length as ``features``.
          Border membership, feature-wise.
        :param d: One dimensional array with the same length as ``features``.
          Slope steepness, feature-wise.
        :return: One dimensional array with the same length as ``features``.
          Computed difference measures, feature-wise.
        """
        return (1.0 / b - 1) * ((np.abs(features - s) / c) ** d)

    def get_membership_functions_xy(
            self,
            npoints: int = 1000,
            epsilon=0.001) -> Tuple[np.ndarray, np.ndarray]:
        """
        Calculate ``npoints`` x-y pairs for each learnt membership function. y
        is the membership at x. These x-y pairs can be used to generate
        membership function plots.

        :param npoints: Number of equidistant x points to compute for each
          membership function.
        :param epsilon: Used to determine the x interval over which the y
          values are calculated. The interval ranges from the left-hand side x
          value for which the membership is ``epsilon`` to the right-hand side
          x value for which the membership is ``epsilon``.
        :return: Tuple (X, Y) of x-axis data and y-axis data of the membership
          functions. X and Y each have the dimensions
          (``npoints``, number of features).
        """
        # xl, xr have shape (N,)
        xl, xr = self._compute_argmemberships(epsilon)

        # x has shape (npoints, N), N: number of feats
        X = np.linspace(xl, xr, npoints)
        Y = self.transform(X)  # y has shape (npoints, N)

        return X, Y

    def _compute_argmemberships(self, mu: float) -> np.ndarray:
        """
        Calculate the arguments (feature values) of the membership functions
        given membership ``mu``. For each feature, the left-hand side
        and right-hand side feature value is returned.

        :param mu: The membership for which to calculate the feature values.
        :return: Array with shape (2, number of features). Array with the
          feature values.

          - ``array[0, feature index]``: Left-hand side feature value.
          - ``array[1, feature index]``: Right-hand side feature value.
        """
        # Some parameters (b*, c*) are only required to be array-like
        # Broadcast all parameters for clarity:
        new_shape = (self.config['nfeatures'],)
        cl = np.broadcast_to(self.get_as_farray('cl'), new_shape)
        cr = np.broadcast_to(self.get_as_farray('cr'), new_shape)
        s = np.broadcast_to(self.get_as_farray('s'), new_shape)
        bl = np.broadcast_to(self.get_as_farray('bl'), new_shape)
        br = np.broadcast_to(self.get_as_farray('br'), new_shape)
        dl = np.broadcast_to(self.get_as_farray('dl'), new_shape)
        dr = np.broadcast_to(self.get_as_farray('dr'), new_shape)

        l_argvs = s - cl * ((-np.log2(mu) / (1.0 / bl - 1)) ** (1.0 / dl))
        r_argvs = s + cr * ((-np.log2(mu) / (1.0 / br - 1)) ** (1.0 / dr))

        return np.stack([l_argvs, r_argvs], axis=0)


class ModifiedFuzzyPatternTransformerModule(Module,
                                            ModifiedFuzzyPatternTransformer):
    """
    Subclass implementing the ml4proflow module aspects of the modified
    fuzzy pattern transformer (:py:class:`ModifiedFuzzyPatternTransformer`).

    :param dfm: The :py:class:`~ml4proflow.modules.DataFlowManager` this module
      is part of.
    :param config: The configuration containing the module parameters. For
      relevant keys, see :py:class:`ModifiedFuzzyPatternTransformer`.
    """
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        Module.__init__(self, dfm, config)
        ModifiedFuzzyPatternTransformer.__init__(
            self, config, self.update_config)

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        """
        :return: The module description required by ml4proflow.
        """
        return {
            "name": "Modified Fuzzy Pattern Transformer",
            "jupyter-gui-cls":
            "ml4proflow_mods.informationfusion.widgets.MFPTransformerWidget",
                "html-description": """
    <h1 id="modified-fuzzy-pattern-transformer-module">Modified Fuzzy Pattern
    Transformer Module</h1>
    <p>This class implements the one-dimensional modified
    fuzzy pattern classifier (MFPC) membership function training.
    The learnt membership functions can also be used 'standalone' as a
    transformation of features to fuzzy membership values. The modified fuzzy
    pattern classifier is published in [LDM04].</p>
    """ + _IFU_LITERATURE_HTML + """
    <h2 id="config-parameters">Configuration Parameters</h2>
    <ul>
    """ + _MFPTRANSFORMER_PARAM_DESC_HTML + """</ul>"""}

    def on_new_data(self, name: str, sender: SourceModule,
                    data: DataFrame) -> None:
        """Transform a new batch of data. If mode is ``'fit'`` the fuzzy
        pattern transformer is fitted with the data before data transformation.

        The following :py:class:`pandas.DataFrame` is pushed to channel
        ``name``:

        +-------+-----------------+-----------------+-----------------+
        |       | feature_0       | ...             | feature_m       |
        +=======+=================+=================+=================+
        | **0** | ``float``-value | ``float``-value | ``float``-value |
        +-------+-----------------+-----------------+-----------------+
        | ...   | ``float``-value | ``float``-value | ``float``-value |
        +-------+-----------------+-----------------+-----------------+
        | **n** | ``float``-value | ``float``-value | ``float``-value |
        +-------+-----------------+-----------------+-----------------+

        :param name: Name of the channel.
        :param sender: Name of the module that sends the data.
        :param data: DataFrame to be processed. The underlying data has the
          shape (batch size, number of features).
        """
        X = data.values
        if self.config['mode'] == 'fit':
            self.fit(X)

        memberships = self.transform(X)  # (nsamples, nfeatures)
        result = DataFrame(data={f'feature_{i}': memberships[:, i]
                                 for i in range(memberships.shape[1])})
        for c in self.config['channels_push']:
            self._push_data(c, result)


def check_array(X: ArrayLike) -> numpy.ndarray:
    """
    This function converts ``X`` to a :py:class:`numpy.ndarray` and performs
    the following checks on it:

    - Array is two-dimensional.
    - Array does not contain any NaNs.
    - Array does not contain complex numbers.

    :param X: The array-like to be converted and checked.
    :raise ValueError: If any of the checks fails.
    :return: The converted and checked array.
    """
    X = np.array(X)
    if X.ndim != 2:
        raise ValueError("An array-like of dimension 2 is expected.")
    if np.any(np.all(np.isnan(X), axis=1)):
        raise ValueError("Training data contains at least one all-NaN column.")
    if np.any(np.iscomplex(X)):
        raise ValueError("Complex data is not supported.")
    return X
