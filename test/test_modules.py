import unittest
from typing import Union, Any, List, Tuple

import numpy as np
import pandas as pd

from ml4proflow import modules as ml4proflow_modules
from ml4proflow_mods.informationfusion import modules


class DataProbeModule(ml4proflow_modules.SinkModule):
    """
    This module stores the last data received for later access.
    """

    def __init__(
            self,
            dfm: ml4proflow_modules.DataFlowManager,
            config: dict[str, Any]
    ):
        ml4proflow_modules.SinkModule.__init__(self, dfm, config)
        self.data_store = {}

    def on_new_data(self, name: str, sender: ml4proflow_modules.SourceModule,
                    data: pd.DataFrame) -> None:
        """
        Store the new data. Existing data for the channel is overwritten.

        :param name: Name of the channel.
        :param sender: Name of the module that sends the data.
        :param data: DataFrame to be stored.
        """
        self.data_store[name] = data

    def get_last_data(self, name: str) -> pd.DataFrame:
        """
        Return the most recent data received on the channel.

        The channel name has to exist and must have received data at least
        once.

        :param name: Name of the channel.
        :return: The most recent data on channel 'name'.
        """
        return self.data_store[name]


class TestMACROModule(unittest.TestCase):
    def setUp(self):
        self.dfm = ml4proflow_modules.DataFlowManager()
        self.probe = DataProbeModule(self.dfm, {})
        self._PROBE_CHANNEL = 'probe_channel'
        self.dfm.create_channel(self._PROBE_CHANNEL)
        self.dfm.register_sink(self._PROBE_CHANNEL, self.probe)
        self._BATCH_SIZE = 4

    def get_dut(
            self,
            config: Union[dict, None] = None
    ) -> modules.MultilayerAttributeBasedConflictReducingObservation:
        if config is None:
            config = {}
        return modules.MultilayerAttributeBasedConflictReducingObservation(
            self.dfm, config)

    def get_X_per_attribute(self) -> Tuple[np.ndarray, np.ndarray]:
        X_1 = np.array([[1.0, 1.0],
                        [0.0, 0.0],
                        [1.0, 0.0],
                        [0.0, 1.0]])
        X_2 = np.array([[0.5, 0.5],
                        [0.5, 1.0],
                        [1.0, 0.5],
                        [0.5, 0.5]])
        assert (len(X_1) == self._BATCH_SIZE)
        assert (len(X_2) == self._BATCH_SIZE)
        return X_1, X_2

    def get_expected_health(self) -> List[float]:
        expected_health = [0.83333333, 0.5, 1.0, 0.16666666]
        assert (len(expected_health) == self._BATCH_SIZE)
        return expected_health

    def test_create_new_module(self):
        dut = self.get_dut()
        self.assertIsInstance(
            dut, modules.MultilayerAttributeBasedConflictReducingObservation)

    def test_default_config_values(self):
        config = {}
        dut = self.get_dut(config=config)
        self.assertEqual(config, dut.config)
        for key in ['nattributes', 'batchsize', 'andness']:
            self.assertIn(key, config)

    def test_module_desc(self):
        dut = self.get_dut()
        desc = dut.get_module_desc()
        for key in ['name', 'jupyter-gui-cls', 'html-description']:
            self.assertIn(key, desc)

    def test_system_health_calculations(self):
        dut = self.get_dut(config={'batchsize': self._BATCH_SIZE})
        Xs = self.get_X_per_attribute()
        health = dut.get_system_health(Xs)
        expected_health = self.get_expected_health()

        for i in range(len(health)):
            self.assertAlmostEqual(health[i], expected_health[i])

    def test_system_health_symmetry(self):
        dut = self.get_dut(config={'batchsize': self._BATCH_SIZE})

        X_1, X_2 = self.get_X_per_attribute()
        health_1 = dut.get_system_health([X_1, X_2])
        health_2 = dut.get_system_health([X_2, X_1])

        for i in range(len(health_1)):
            self.assertAlmostEqual(health_1[i], health_2[i])

    def test_new_data(self):
        dut = self.get_dut()
        # Prevent channel from being created twice:
        dut.config['channels_push'] = [self._PROBE_CHANNEL]

        X_1, X_2 = self.get_X_per_attribute()
        health = self.get_expected_health()

        # Check that module is not sending too early:
        dut.on_new_data('x1', None, pd.DataFrame(data=X_1[:1]))
        self.assertNotIn(self._PROBE_CHANNEL, self.probe.data_store)

        # Check module results:
        dut.on_new_data('x2', None, pd.DataFrame(data=X_2[:1]))
        data = self.probe.get_last_data(self._PROBE_CHANNEL).values.ravel()
        self.assertEqual(len(data), 1)
        self.assertAlmostEqual(data[0], health[0])
        for i in range(1, len(health)):
            dut.on_new_data('x1', None, pd.DataFrame(data=X_1[i:i + 1]))
            dut.on_new_data('x2', None, pd.DataFrame(data=X_2[i:i + 1]))
            data = self.probe.get_last_data(self._PROBE_CHANNEL).values.ravel()
            self.assertEqual(len(data), 1)
            self.assertAlmostEqual(data[0], health[i])

    def test_new_data_errors(self):
        dut = self.get_dut()
        # Prevent channel from being created twice:
        dut.config['channels_push'] = [self._PROBE_CHANNEL]

        X_1, X_2 = self.get_X_per_attribute()

        # Wrong batch size:
        self.assertRaises(
            ValueError,
            lambda: dut.on_new_data('x1', None, pd.DataFrame(data=X_1))
        )

        # Too many channels:
        dut.on_new_data('x1', None, pd.DataFrame(data=X_1[:1]))
        dut.on_new_data('x2', None, pd.DataFrame(data=X_2[:1]))
        self.assertRaises(
            ValueError,
            lambda: dut.on_new_data('x3', None, pd.DataFrame(data=X_1[:1]))
        )

    def test_andness(self):
        macro_mod = modules.MultilayerAttributeBasedConflictReducingObservation
        applied_andness = macro_mod.get_applied_andness([0.5, 0.5])
        self.assertAlmostEqual(applied_andness, 0.5)


class TestMACROAttributeModule(unittest.TestCase):
    def setUp(self):
        self.dfm = ml4proflow_modules.DataFlowManager()
        self.probe = DataProbeModule(self.dfm, {})
        self._PROBE_CHANNEL = 'probe_channel'
        self.dfm.create_channel(self._PROBE_CHANNEL)
        self.dfm.register_sink(self._PROBE_CHANNEL, self.probe)

    def get_dut(self,
                config: Union[dict, None] = None) -> modules.MACROAttribute:
        if config is None:
            config = {}
        return modules.MACROAttribute(self.dfm, config)

    @staticmethod
    def get_X():
        X = np.array([[6, -5],
                      [4.5, -6],
                      [3, -4.5],
                      [5.7, -3],
                      [5, -5.7]])
        return X

    @staticmethod
    def get_expected_memberships_post_fit():
        expected_result = np.array([[0.5, 1.0],
                                    [0.9576032807, 0.5],
                                    [0.5, 0.9576032807],
                                    [0.7120250978, 0.5],
                                    [1.0, 0.7120250978]])
        return expected_result

    @staticmethod
    def get_expected_result_post_fit():
        # Expected MFPTransformer membership values using X as input:
        # 0.5000000000, 1.0000000000
        # 0.9576032807, 0.5000000000
        # 0.5000000000, 0.9576032807
        # 0.7120250978, 0.5000000000
        # 1.0000000000, 0.7120250978
        expected_result = np.array([[0.875, 0.5],
                                    [0.8432024606, 0.5],
                                    [0.8432024606, 0.5],
                                    [0.6590188234, 0.5],
                                    [0.9585352279, 0.7120250978]])
        return expected_result

    @staticmethod
    def get_expected_result_no_fit():
        # Expected MFPTransformer membership values using X as input:
        # 1.45519152e-11, 2.98023224e-08
        # 8.01941314e-07, 1.45519152e-11
        # 1.95312500e-03, 8.01941314e-07
        # 1.65781262e-10, 1.95312500e-03
        # 2.98023224e-08, 1.65781262e-10
        expected_result = np.array([[4.44956677e-16, 9.99999970e-01],
                                    [3.21578275e-13, 9.99999198e-01],
                                    [1.91047848e-06, 9.98046076e-01],
                                    [1.90734928e-06, 9.98046875e-01],
                                    [4.53984285e-16, 9.99999970e-01]])
        return expected_result

    def test_create_new_module(self):
        dut = self.get_dut()
        self.assertIsInstance(dut, modules.MACROAttribute)
        self.assertIsInstance(dut.mfpc_transformer,
                              modules.ModifiedFuzzyPatternTransformer)

    def test_default_config_values(self):
        config = {}
        dut = self.get_dut(config=config)
        self.assertEqual(config, dut.config)
        self.assertIn('mode', config)

    def test_config_vals_check(self):
        # Fit mode:
        config = {'mode': 'fit'}
        dut = self.get_dut(config=config)
        self.assertEqual(dut.config['mode'], 'fit')
        # Predict mode:
        config = {'mode': 'predict'}
        dut = self.get_dut(config=config)
        self.assertEqual(dut.config['mode'], 'predict')
        # Unknown mode:
        config = {'mode': 'something unknown'}
        self.assertRaises(ValueError, lambda: self.get_dut(config=config))

    def test_module_desc(self):
        dut = self.get_dut()
        desc = dut.get_module_desc()
        for key in ['name', 'jupyter-gui-cls', 'html-description']:
            self.assertIn(key, desc)

    # Fit method of MFPTransformer is tested in MFPTransformer test class.
    def test_fit(self):
        dut = self.get_dut()
        X = self.get_X()

        obj = dut.fit(X)
        self.assertIsInstance(obj, modules.MACROAttribute)

    def test_fuse_samples(self):
        config = {'parmethod': 'median'}
        dut = self.get_dut(config)
        X = self.get_X()
        dut.fit(X)

        result = dut.fuse_samples(X)
        expected_result = self.get_expected_result_post_fit()
        self.assertEqual(len(result), len(expected_result))
        for i in range(len(expected_result)):
            self.assertAlmostEqual(result[i, 0], expected_result[i, 0])
            self.assertAlmostEqual(result[i, 1], expected_result[i, 1])

    def test_new_data_fit_mode(self):
        dut = self.get_dut(config={'mode': 'fit'})
        # Prevent channel from being created twice:
        dut.config['channels_push'] = [self._PROBE_CHANNEL]

        dut.on_new_data('x', None, pd.DataFrame(data=self.get_X()))
        result = self.probe.get_last_data(self._PROBE_CHANNEL)
        expected_result = self.get_expected_result_post_fit()
        self.assertIn('membership', result.columns)
        self.assertIn('importance', result.columns)

        self.assertEqual(len(result), len(expected_result))
        for i in range(len(expected_result)):
            self.assertAlmostEqual(result.loc[i, 'membership'],
                                   expected_result[i, 0])
            self.assertAlmostEqual(result.loc[i, 'importance'],
                                   expected_result[i, 1])

    def test_new_data_predict_mode(self):
        dut = self.get_dut(config={'mode': 'predict'})
        # Prevent channel from being created twice:
        dut.config['channels_push'] = [self._PROBE_CHANNEL]

        dut.on_new_data('x', None, pd.DataFrame(data=self.get_X()))
        result = self.probe.get_last_data(self._PROBE_CHANNEL)
        expected_result = self.get_expected_result_no_fit()
        self.assertIn('membership', result.columns)
        self.assertIn('importance', result.columns)

        self.assertEqual(len(result), len(expected_result))
        for i in range(len(expected_result)):
            self.assertAlmostEqual(result.loc[i, 'membership'],
                                   expected_result[i, 0])
            self.assertAlmostEqual(result.loc[i, 'importance'],
                                   expected_result[i, 1])


class TestModifiedFuzzyPatternTransformer(unittest.TestCase):

    @staticmethod
    def get_X():
        X = np.array([[6, -5],
                      [4.5, -6],
                      [3, -4.5],
                      [5.7, -3],
                      [5, -5.7]])
        return X

    @staticmethod
    def get_expected_results_median():
        return np.array([[0.5, 1.0],
                         [0.9576032806985737, 0.5],
                         [0.5, 0.9576032806985737],
                         [0.7120250977985357, 0.5],
                         [1.0, 0.7120250977985357]])

    @staticmethod
    def get_dut(config: Union[dict, None] = None
                ) -> modules.ModifiedFuzzyPatternTransformer:
        if config is None:
            config = {}
        return modules.ModifiedFuzzyPatternTransformer(config=config)

    def test_create_new_instance(self):
        dut = self.get_dut()
        self.assertIsInstance(dut, modules.ModifiedFuzzyPatternTransformer)
        self.assertEqual(dut.update_config, dut.update_config_default)

    def test_default_config_values(self):
        config = {}
        dut = self.get_dut(config=config)
        self.assertEqual(config, dut.config)
        for key in ['bl', 'br', 'dl', 'dr', 's', 'cl', 'cr', 'mode',
                    'pcel', 'pcer', 'parmethod', 'epsilon', 'nfeatures']:
            self.assertIn(key, config)

    def test_pars_symmetrical(self):
        dut = self.get_dut()
        # Positive test:
        for key in ['bl', 'dl', 'pcel', 'br', 'dr', 'pcer']:
            dut.update_config(key, 0.5)

        self.assertTrue(dut._parameters_symmetrical())

        # Negative test:
        dut.update_config('bl', 0.4)
        self.assertFalse(dut._parameters_symmetrical())
        dut.update_config('bl', 0.5)

        dut.update_config('dl', 0.4)
        self.assertFalse(dut._parameters_symmetrical())
        dut.update_config('dl', 0.5)

        dut.update_config('pcel', 0.4)
        self.assertFalse(dut._parameters_symmetrical())

    def assert_identical_results_fit(self, config, expected_results: dict):
        dut = self.get_dut(config)
        X = self.get_X()

        dut.fit(X)
        self.assert_identical_results(config, expected_results)

    def assert_identical_results(self, results: dict, expected_results: dict):
        for key, expected_result in expected_results.items():
            result = results[key]
            self.assertEqual(len(result), len(expected_result))
            for i in range(len(expected_result)):
                self.assertAlmostEqual(result[i], expected_result[i])

    def test_fit_median(self):
        config = {'parmethod': 'median'}
        expected_results = {'s': [5, -5],
                            'cl': [5 - 3, 6 - 5],
                            'cr': [6 - 5, 5 - 3]}
        self.assert_identical_results_fit(config, expected_results)

    def test_fit_mean(self):
        config = {'parmethod': 'mean'}
        expected_results = {'s': [4.84, -4.84],
                            'cl': [4.84 - 3, 6 - 4.84],
                            'cr': [6 - 4.84, 4.84 - 3]}
        self.assert_identical_results_fit(config, expected_results)

    def test_fit_classical(self):
        config = {'parmethod': 'classical'}
        expected_results = {'s': [4.5, -4.5],
                            'cl': [1.5, 1.5],
                            'cr': [1.5, 1.5]}
        self.assert_identical_results_fit(config, expected_results)

    def test_check_inputs(self):
        dut = self.get_dut()
        config_copy = dut.config.copy()
        dut._check_inputs()
        for key in config_copy.keys():
            self.assertEqual(config_copy[key], dut.config[key])

    def test_transform(self):
        dut = self.get_dut()
        X = self.get_X()
        dut.fit(X)

        result = dut.transform(X)
        # Function to calculate distance with bl, br = 0.5; dl, dr = 2:
        # lambda c, x, x0: (1 / 0.5 - 1) * (np.abs(x - x0)/c)**2
        # membership given by 2**-distance
        expected_result = self.get_expected_results_median()

        self.assertEqual(result.shape, expected_result.shape)
        for value, expected_value in zip(result.ravel(),
                                         expected_result.ravel()):
            self.assertAlmostEqual(value, expected_value)

    def test_get_membership_func(self):
        dut = self.get_dut()
        X = self.get_X()
        dut.fit(X)

        expected_result = self.get_expected_results_median()
        func_X, func_Y = dut.get_membership_functions_xy()

        for X, Y in zip(X, expected_result):  # Iteration over rows (samples)
            # Find the closest X:
            idx = np.argmin(np.abs(X - func_X), axis=0)
            self.assertAlmostEqual(func_Y[idx[0], 0], Y[0], places=2)
            self.assertAlmostEqual(func_Y[idx[1], 1], Y[1], places=2)


class TestModifiedFuzzyPatternTransformerModule(unittest.TestCase):
    def setUp(self):
        self.dfm = ml4proflow_modules.DataFlowManager()
        self.probe = DataProbeModule(self.dfm, {})
        self._PROBE_CHANNEL = 'probe_channel'
        self.dfm.create_channel(self._PROBE_CHANNEL)
        self.dfm.register_sink(self._PROBE_CHANNEL, self.probe)

    @staticmethod
    def get_X():
        return TestModifiedFuzzyPatternTransformer.get_X()

    @staticmethod
    def get_expected_result():
        return (
            TestModifiedFuzzyPatternTransformer.get_expected_results_median()
        )

    def get_dut(self, config: Union[dict, None] = None
                ) -> modules.ModifiedFuzzyPatternTransformerModule:
        if config is None:
            config = {}
        return modules.ModifiedFuzzyPatternTransformerModule(
            self.dfm, config)

    def test_create_new_module(self):
        dut = self.get_dut()
        self.assertIsInstance(dut,
                              modules.ModifiedFuzzyPatternTransformerModule)

    def test_module_desc(self):
        dut = self.get_dut()
        desc = dut.get_module_desc()
        for key in ['name', 'jupyter-gui-cls', 'html-description']:
            self.assertIn(key, desc)

    def test_new_data(self):
        dut = self.get_dut()
        # Prevent channel from being created twice:
        dut.config['channels_push'] = [self._PROBE_CHANNEL]

        dut.on_new_data('x', None, pd.DataFrame(data=self.get_X()))
        result = self.probe.get_last_data(self._PROBE_CHANNEL).values
        expected_result = self.get_expected_result()

        self.assertEqual(result.shape, expected_result.shape)
        for value, expected_value in zip(result.ravel(),
                                         expected_result.ravel()):
            self.assertAlmostEqual(value, expected_value)


class ModifiedFuzzyPatternClassifier(unittest.TestCase):
    def setUp(self):
        self.dfm = ml4proflow_modules.DataFlowManager()
        self.probe = DataProbeModule(self.dfm, {})
        self._PROBE_CHANNEL = 'probe_channel'
        self.dfm.create_channel(self._PROBE_CHANNEL)
        self.dfm.register_sink(self._PROBE_CHANNEL, self.probe)

    @staticmethod
    def get_X():
        return TestModifiedFuzzyPatternTransformer.get_X()

    @staticmethod
    def get_expected_inlier_score():
        return np.array([0, -0.02119835965071315, -0.02119835965071315,
                         -0.14398745110073213, 0.21202509779853573])

    @staticmethod
    def get_expected_class():
        return np.array([1, -1, -1, -1, 1])

    def get_dut(self, config: Union[dict, None] = None
                ) -> modules.ModifiedFuzzyPatternClassifier:
        if config is None:
            config = {}
        return modules.ModifiedFuzzyPatternClassifier(
            self.dfm, config)

    def assert_identical_results(self, result: np.ndarray,
                                 expected_result: np.ndarray):
        self.assertEqual(result.shape, expected_result.shape)
        for value, expected_value in zip(result.ravel(),
                                         expected_result.ravel()):
            self.assertAlmostEqual(value, expected_value)

    def test_create_new_module(self):
        dut = self.get_dut()
        self.assertIsInstance(dut,
                              modules.ModifiedFuzzyPatternClassifier)
        self.assertIsInstance(dut.mfpc_transformer,
                              modules.ModifiedFuzzyPatternTransformer)

    def test_default_config_values(self):
        config = {}
        dut = self.get_dut(config=config)
        self.assertEqual(config, dut.config)
        for key in ['threshold', 'mode']:
            self.assertIn(key, config)

    def test_module_desc(self):
        dut = self.get_dut()
        desc = dut.get_module_desc()
        for key in ['name', 'jupyter-gui-cls', 'html-description']:
            self.assertIn(key, desc)

    def test_new_data(self):
        dut = self.get_dut()
        # Prevent channel from being created twice:
        dut.config['channels_push'] = [self._PROBE_CHANNEL]

        dut.on_new_data('x', None, pd.DataFrame(data=self.get_X()))
        result = self.probe.get_last_data(self._PROBE_CHANNEL).values

        self.assert_identical_results(result.squeeze(),
                                      self.get_expected_class())

    # Fit method of MFPTransformer is tested in MFPTransformer test class.
    def test_fit(self):
        dut = self.get_dut()
        X = self.get_X()

        obj = dut.fit(X)
        self.assertIsInstance(obj, modules.ModifiedFuzzyPatternClassifier)

    def test_predict(self):
        dut = self.get_dut()
        X = self.get_X()
        dut.fit(X)
        result = dut.predict(X)

        self.assert_identical_results(result.squeeze(),
                                      self.get_expected_class())

    def test_decision_function(self):
        dut = self.get_dut()
        X = self.get_X()
        dut.fit(X)
        result = dut.decision_function(X)

        self.assert_identical_results(result.squeeze(),
                                      self.get_expected_inlier_score())

    def test_score_samples(self):
        dut = self.get_dut()
        X = self.get_X()
        dut.fit(X)
        result = dut.score_samples(X)
        threshold = dut.config['threshold']

        self.assert_identical_results(
            result.squeeze(), self.get_expected_inlier_score() + threshold
        )


class TestModuleFunctions(unittest.TestCase):
    """The tests of the module functions are done here."""

    def test_fuzzified_bal_tlcs(self):
        data_provider = TestMACROAttributeModule
        memberships = data_provider.get_expected_memberships_post_fit()
        expected_result = data_provider.get_expected_result_post_fit()

        fut = modules.fuzzified_bal_tlcs
        for i in range(len(expected_result)):
            self.assertAlmostEqual(fut(memberships[i])[0],
                                   expected_result[i, 0])
            self.assertAlmostEqual(fut(memberships[i])[1],
                                   expected_result[i, 1])

    def test_fuzzified_bal_tlcs_errors(self):
        fut = modules.fuzzified_bal_tlcs
        self.assertRaises(ValueError, lambda: fut(3.0))
        self.assertRaises(ValueError, lambda: fut([[3.0]]))
        self.assertRaises(ValueError, lambda: fut([0.5, -0.01]))
        self.assertRaises(ValueError, lambda: fut([0.5, 1.01]))


if __name__ == '__main__':
    unittest.main()
