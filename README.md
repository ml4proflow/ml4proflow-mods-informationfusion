# ml4proflow-mods-informationfusion

This package provides information fusion modules for ml4proflow.

[![Tests Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/tests-badge.svg?job=gen-cov)](#CI)
[![Coverage Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/coverage-badge.svg?job=gen-cov)](#CI)
[![Flake8 Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/flake8-badge.svg?job=gen-cov)](#CI)
[![mypy errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/mypy.svg?job=gen-cov)](#CI)
[![mypy strict errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-informationfusion/-/jobs/artifacts/master/raw/mypy_strict.svg?job=gen-cov)](#CI)
------------
## Provided Modules 
This package contains the following ml4proflow modules:

- **Modified Fuzzy Pattern Transformer**:

  This module learns a transformation from feature space to fuzzy membership
  space for each feature. The provided options for membership function training
  are, for example, described in [Mön17]_.

- **Modified Fuzzy Pattern Classifier**:

  This module implements the modified fuzzy pattern classifier published in
  [LDM04]_.
- **Multilayer Attribute-based Conflict-reducing Observation Attribute**:

  This module implements the attribute layer fusion for the
  Multilayer Attribute-based Conflict-reducing Observation (MACRO) model
  published in [Mön17]_.
- **Multilayer Attribute-based Conflict-reducing Observation**:

  This module implements the system layer fusion for the MACRO model published 
  in [Mön17]_.

For a detailed theoretical overview of the algorithms, please refer to
[Mön17]_.

## Prerequisites
- [pandas](https://pandas.pydata.org/)
- [numpy](https://numpy.org/)
- [ml4proflow](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow)
- [ipywidgets](https://ipywidgets.readthedocs.io/) (for Jupyter GUI)
- [matplotlib](https://matplotlib.org/) (for Jupyter GUI)

## Installation
Activate your virtual environment (optionally), clone this repository, and
install the package with pip:
```console 
$ pip install .
```

## Contribution
For development, install this repository in editable mode with pip:
```console 
$ pip install -e .
```
